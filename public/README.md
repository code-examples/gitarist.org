# [gitarist.org](https://gitarist.org) source codes

<br/>

### Run gitarist.org on localhost

    # vi /etc/systemd/system/gitarist.org.service

Insert code from gitarist.org.service
  
 # systemctl enable gitarist.org.service # systemctl start gitarist.org.service # systemctl status gitarist.org.service

http://localhost:4014
