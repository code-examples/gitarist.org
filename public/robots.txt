---
layout: null
permalink: robots.txt
---

User-agent: *
Allow: /
Host: https://gitarist.org
Sitemap: https://gitarist.org/sitemap.xml
